// CRUD Operations in MongoDB (As discussed)
// To execute the code in Robo3T, press F5 or Control + Enter
// C - CREATE (Or INSERT)
	// INSERT ONE: To insert one document, we have used the method "insertOne"

	/*Syntax: 
		db.collectionName.insertOne({object});
	*/

	db.users.insertOne({
		"firstName": "John",
		"lastName": "Smith",
		"age": 21,
		"contact": {
			phone: "238293847",
			email: "johnsmith@gmail.com"
		}
	});
	// INSERT MANY: To insert multiple documents, we can use insertMany()
	/*
		Syntax:
			db.collectionName.insertMany([
		{ "objectA" },
		{ "objectB" }
	]);
	*/
	db.users.insertMany([ { 
		"firstName": "John",
		"lastName": "Doe", 
		"age": 21, 
		"contact": {
			phone: "238293847",
			email: "johnsmith@gmail.com" 
		},
		"course": ["Python", "React", "PHP"
		],
		"department": "none"
	},
		{ 	
		"firstName": "Jane",
		"lastName": "Doe",
		"age": 21, 
		"contact": {
			phone: "990299345",
			email: "janedoe@gmail.com" 
		},
		"course": ["Python", "React", "PHP"
		],
		"department": "none"
	}
	]);

// R - READ
	// FIND: To get all the inserted users, we can use find()
	/*
		Syntax:
			db.collectionName.find();
	*/
	db.users.find();

	// FIND SPECIFIC: To find documents with a specified condition, an object should be passed to the find().
	/*
		Syntax:
			db.collectionName.find({"objectA": "valueA"});
	*/
	db.users.find({ "lastName": "Doe" })
	// - This will find all users with Doe on their lastName

// Find multiple parameters
/*
	Syntax:
		db.collectionName.find({"fieldA": valueA, "fieldB": "valueB"});
*/
	
		db.collectionName.find({"firstName": "Doe", "age": 21 });


// U- UPDATE
	// UPDATE ONE:
	/*
		-Syntax:
			db.collectionName.updateOne(
			{
				criteria
			},
			{
				$set: {field:value}
			}
		);
	*/
	db.users.updateOne(
		{
			"_id": ObjectId("62b54b31b55e350b5950bb76")
		},
		{
			$set: {
				"email": "johnsmith@gmail.com"
			}
		}
	);
	// - The first argument contains the identifier on which documents to update and the second argument contains the properties to be updated
	// - Since email did not exist before the update, updateOne() will simply add the email property to the selected document. 
	
	// UPDATE MANY: 
	/*
		Syntax:
			db.users.updateMany(
		{criteria},{$set: {"field": "value"}
		}
	);

	*/
	db.users.updateMany(
		{
			"lastName": "Doe"
		},
		{
			$set: {
				"isAdmin": false
			}
		}
	);
	// - Now, all documents with last name of "Doe" has a property of isAdmin with a value of false. 
// D - DELETE
// DELETE MANY: To update multiple documents, we can use deleteMany()
db.users.deleteMany({ "lastName": "Doe" });
// - Be careful when deleting data as you may delete data accidentally! 
// DELETE ONE: We can use deleteOne instead when we're sure we want to delete only one document.
db.users.deleteOne({ "_id": ObjectId("62b54b31b55e350b5950bb76") });



// How we operate on MongoDB and Robo3T 
// 1. Right click on the new connections. Then on the drop down choices, click "Create Database"
// 2. Decide on the name of the database
// 3. Right click on the new made database. Click on "Open Shell"
// 4. Type the code on the field where you are allowed to type the code
// 5. To execute the code, press F5 or Control + Enter
// What we did on MongoDB
// 1. We registered through gmail
// 2. We have chosen Amazon and Singapore respectively. After that, we have decided on the name of the cluster
// 3. We have added an IP on the Network Access. We have clicked "Allow access from anywhere"
// 4. We have set our username and password on the Quickstart


// Find + Pretty method
/*
	Syntax:
		db.collectionName.find({ field:value}).pretty();

*/

// 1. insert initial documents
db.users.insert({
	firstName: "Test",
	lastName: "test",
	age: 0,
	contact: {
		phone: "00000000",
		email: "testemail@yahoo.com"
	},
	course: [],
	department: "none"
});

// 2. update the document

db.users.updateOne({
	firstName: "Test"
},
{
	$set:{
		firstName: "Bill",
		lastName: "Gates",
		age: "65",
		contact: {
			phone: "87654321",
			email: "bill@gmail.com"
		},
		course: ["PHP", "Laravel", "HTML"],
		department: "Operations",
		status: "active"
}
};

db.users.find({firstName: "Bill"}).pretty();


// Replace One
/*
	-replaces the whole documents
	Syntax:
		db.collectionName.replaceOne(
			{criteria}
			{
				fieldA: "ValueA",
				fieldA: "ValueA",
			}
		);
*/

// Advance queries
/*
	-Retrieving data with complex data structure is also a good skill for any devloper to have
	-Real world examples of data can be as complex as having two or more layers of nested objects
	-Learning that we are able to retrieve any information that we would need in our application

*/

// Query an embedded document
// an embedded document are those of documents that contain a document inside a document

db.users.find({
	contact: {
		phone: "998937293",
		email: "stephenhawkins@gmail.com"
	}
}).pretty();

// Query on nested field
db.users.find({
	"contact.email": "janedoe@gmail.com"
})pretty();

// Query an array with exact elements
db.users.find({course: ["CSS", "JavaScript", "Python"]})pretty();

// Query an array without refard to order
db.users.find({{$all: ["React", "Python"]}}).pretty();

// Query an embedded array
dc.users.insertOne({
	nameArr: [
		{
			nameA: "Juan"
		},
		{
			nameB: "Tamad"
		}
	]
});
db.users.find({
	nameArr:
	{
		nameA: "Juan"
	}
}).pretty();
